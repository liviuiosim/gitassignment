package ro.ase.csie.iosimliviu;

public class Applicant implements User{
	String name;
	int age;
	
	public Applicant(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	@Override
	public void displayInfo() {
		System.out.println("Name: " + this.name + " Age: " + this.age);
		
	}
}
